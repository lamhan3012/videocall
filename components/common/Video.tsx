import React, { useRef } from 'react'

interface VideoProps {
  userId: string,
  stream: MediaStream
}

export const Video = ({ userId, stream }: VideoProps) => {
  const refVideo = useRef<HTMLVideoElement>(null)
  React.useEffect(() => {
    if (refVideo.current) refVideo.current.srcObject = stream;
  }, [stream]);

  return (
    <div>
      <h3>{userId}</h3>
      <video ref={refVideo} autoPlay></video>
    </div>
  )
}
