import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { faGreaterThan } from '@fortawesome/free-solid-svg-icons'
import { useVideoCallActionContext } from '../../../../stores/videoCallAction/videoCallAction.context'



const ChatVideoCall = () => {
  const [state, actions] = useVideoCallActionContext()
  return (
    <div className={`chats ${state.showChat ? "active" : ""}`}>
      <div className="chats-title">
        <h3>Tin nhắn trong cuộc gọi</h3>
      </div>
      <div className="chats-notify">
        <p>Tin nhắn sẽ chỉ hiển thị với những người tham gia cuộc gọi và sẽ bị xóa khi cuộc gọi kết thúc.</p>
      </div>
      <div className="chats-list">

      </div>
      <div className="chats-input">
        <input type="text" />
        <div className="chats-enter">
          <FontAwesomeIcon icon={faGreaterThan} />
        </div>
      </div>
    </div>
  )
}

export default ChatVideoCall
