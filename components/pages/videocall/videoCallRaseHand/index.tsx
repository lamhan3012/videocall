import React from 'react'
import animationData from '../../../../json/67604-rase-hand-icon.json'
import Lottie from 'react-lottie'
import { useVideoCallActionContext } from '../../../../stores/videoCallAction/videoCallAction.context'

interface Props {

}

const VideoCallRaseHandReceive = (props: Props) => {
  const [state, actions] = useVideoCallActionContext()
  console.log(state.isRaseHandReceive)
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice"
    }
  };
  return (
    <div className="videoCall-raseHand" style={{ display: `${state.isRaseHandReceive ? "block" : "none"}` }}>
      <Lottie
        options={defaultOptions}
        height={70}
        width={60}
      />
    </div>
  )
}

export default VideoCallRaseHandReceive
