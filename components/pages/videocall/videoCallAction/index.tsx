import React from 'react'
import { faMicrophone, faMicrophoneAltSlash, faCamera, faDesktop, faRecordVinyl, faEllipsisV, faFistRaised, faCommentDots, faPhoneSlash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useVideoCallActionContext } from '../../../../stores/videoCallAction/videoCallAction.context'

interface VideoCallActionProps {
  toggleMic: () => void,
  toggleCamera: () => void,
  shareScreen: () => void,
  handleRaseHand: () => void,
  handleRecord: () => void,
  handleEndCall: () => void
}
const VideoCallAction = (props: VideoCallActionProps) => {
  const { toggleMic, toggleCamera, shareScreen, handleRaseHand, handleRecord, handleEndCall } = props
  const [state, actions] = useVideoCallActionContext()

  return (
    <div className="videoCall-action">
      <div className="videoCal-action_left">
        <div className={`videoCal-action_items ${state.isMute && "active"}`} onClick={() => { toggleMic(), actions.setMute(!state.isMute) }}>
          {state.isMute ? <FontAwesomeIcon icon={faMicrophoneAltSlash} /> : <FontAwesomeIcon icon={faMicrophone} />}
        </div>
        <div className="videoCal-action_items" onClick={() => {
          toggleCamera();
          actions.setCamera(!state.isShowCamera)
        }}>
          {state.isShowCamera ? <FontAwesomeIcon icon={faCamera} /> : <FontAwesomeIcon icon={faMicrophone} />}
        </div>
        <div className="videoCal-action_items" onClick={handleRaseHand}>
          <FontAwesomeIcon icon={faFistRaised} />
        </div>
        <div className="videoCal-action_items" onClick={shareScreen}>
          <FontAwesomeIcon icon={faDesktop} />
        </div>
      </div>
      <div className="videoCal-action_right">
        <div className="videoCal-action_items" onClick={handleRecord}>
          <FontAwesomeIcon icon={faRecordVinyl} />
        </div>
        <div className="videoCal-action_items" onClick={() => actions.setShowChat(!state.showChat)}>
          <FontAwesomeIcon icon={faCommentDots} />
        </div>
        <div className="videoCal-action_items" onClick={() => handleEndCall()}>
          <FontAwesomeIcon icon={faPhoneSlash} style={{ color: "red" }} />
        </div>
        <div className="videoCal-action_items">
          <FontAwesomeIcon icon={faEllipsisV} />
        </div>
      </div>
    </div>
  )
}

export default VideoCallAction
