import { createStore, createHook, Action } from 'react-sweet-state';
type State = {
	isMute: boolean;
	isShowCamera: boolean;
	isRaseHandClient: boolean;
	isRaseHandReceive: boolean;
	showChat: boolean;
	record: boolean;
};
type Actions = typeof actions;

const initialState: State = {
	isMute: false,
	isShowCamera: false,
	isRaseHandClient: false,
	isRaseHandReceive: false,
	showChat: false,
	record: false
};

const actions = {
	setMute:
		(mute: boolean): Action<State> =>
		({ setState, getState }) => {
			setState({ ...getState(), isMute: mute });
		},
	setCamera:
		(isCamera: boolean): Action<State> =>
		({ setState, getState }) => {
			setState({ ...getState(), isShowCamera: isCamera });
		},
	setRaseHandClient:
		(isRaseHand: boolean): Action<State> =>
		({ setState, getState }) => {
			setState({ ...getState(), isRaseHandClient: isRaseHand });
		},
	setRaseHandReceive:
		(isRaseHand: boolean): Action<State> =>
		({ setState, getState }) => {
			console.log(isRaseHand);
			setState({ ...getState(), isRaseHandReceive: isRaseHand });
		},
	setShowChat:
		(showChat: boolean): Action<State> =>
		({ setState, getState }) => {
			console.log(showChat);
			setState({ ...getState(), showChat: showChat });
		},
		setRecord:
		(record: boolean): Action<State> =>
		({ setState, getState }) => {
			console.log(record);
			setState({ ...getState(), record: record });
		}
};

const Store = createStore<State, Actions>({
	initialState,
	actions,
	name: 'VideoCallAction'
});

export const useVideoCallActionContext = createHook(Store);
