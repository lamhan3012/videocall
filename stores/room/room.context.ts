import {
	createStore,
	createSubscriber,
	createHook,
	Action
} from 'react-sweet-state';
type State = { idRoom: string };
type Actions = typeof actions;

const initialState: State = {
	idRoom: ''
};

const actions = {
	setIdRoom:
		(newId: string): Action<State> =>
		({ setState, getState }) => {
			setState({ ...getState(), idRoom: newId });
		}
};

const Store = createStore<State, Actions>({
	initialState,
	actions,
	name: 'room'
});

export const useRoomContext = createHook(Store);
