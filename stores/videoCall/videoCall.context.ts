import { createStore, createHook, Action } from 'react-sweet-state';
type State = {
	pcRef: RTCPeerConnection | null;
	localVideoRef: HTMLVideoElement | null;
	remoteVideoRef: HTMLInputElement | null;
};
type Actions = typeof actions;

const initialState: State = {
	pcRef: null,
	localVideoRef: null,
	remoteVideoRef: null
};

const actions = {
	setLocalVideo:
		(stream: MediaStream): Action<State> =>
		({ setState, getState }) => {
			const stateClone = { ...getState() };
      if(stateClone.localVideoRef)
      {
        stateClone.localVideoRef.srcObject = stream;
      }
      setState(stateClone)
		}
};

const Store = createStore<State, Actions>({
	initialState,
	actions,
	name: 'VideoCall'
});

export const useVideoCallContext = createHook(Store);
