import {
	createStore,
	createSubscriber,
	createHook,
	Action
} from 'react-sweet-state';
import { io, Socket } from 'socket.io-client';
type State = { socket: Socket | null };
type Actions = typeof actions;

const initialState: State = {
	socket: null
};

const actions = {
	connect:
		(host: string): Action<State> =>
		({ setState, getState }) => {
			const socket = io(host);
      console.log(`LHA:  ===> file: socket.context.ts ===> line 20 ===> socket`, socket)
			setState({ socket });
		},
	disconnect:
		(): Action<State> =>
		({ setState, getState }) => {
			getState().socket?.disconnect();
		}
};

const Store = createStore<State, Actions>({
	initialState,
	actions,
	name: 'socket'
});

export const useSocketContext = createHook(Store);
