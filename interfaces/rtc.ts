export enum TypeTrack {
	audio = 0,
	video = 1
}

export enum TypeGetMedia {
	userMedia = 0,
	displayMedia = 1
}
