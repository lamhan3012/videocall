export interface ActionResponseInterface {
	error: boolean;
	message: string;
}
