import faker from 'faker';
import { ActionResponseInterface } from '../interfaces/common';

faker.locale = 'vi';

export function timeout(ms: number) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

export const RandomString = () => faker.name.title();
export const RandomFloatNumber = () =>
	Number.parseFloat(`${Math.random()}`).toFixed(2);
export const RandomID = () => Math.round(Math.random() * 1000);

export const ACTION_RESPONSE = (
	error: boolean = false,
	message: string = ''
): ActionResponseInterface => {
	return { error, message };
};

/** SAVE RAW DATA -> STRINGIFY DATA -> TO LOCAL STORAGE
 * @param key    localStorage key name to get Value from localStorage
 * @param value  Object|Array|Boolean|String|Number data to save underneath the key name
 */
export const saveToLocalStorage = (key: string, value: any) =>
	window.localStorage.setItem(key, JSON.stringify(value));

export const getFromLocalStorage = (key: string): any => {
	const value = window.localStorage.getItem(key);
	if (!value) return null;

	return JSON.parse(value);
};
