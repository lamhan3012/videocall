import { TypeGetMedia, TypeTrack } from '../interfaces/rtc';


export const CONSTRAIN_USER_MEDIA={
	video: true,
	audio: true,
}

export const CONSTRAIN_DISPLAY_MEDIA={
	video: {
		// cursor: "always"
	},
	audio: {
		echoCancellation: true,
		// noiseSuppression: true,
		sampleRate: 44100
	}
}

const getDisplayMedia = async (displayMediaOptions: any) => {
	try {
		if (navigator.mediaDevices.getDisplayMedia) {
			const stream: MediaStream = await navigator.mediaDevices.getDisplayMedia(
				displayMediaOptions
			);
			return stream;
		}
		return null;
	} catch (err) {
		console.error('Error: ' + err);
		return null;
	}
};

const getUserMedia = async (
	userMediaOptions: any
): Promise<MediaStream | undefined> => {
	try {
		if (navigator.mediaDevices.getUserMedia) {
			const stream: MediaStream = await navigator.mediaDevices.getUserMedia(
				userMediaOptions
			);
			return stream;
		}
	} catch (err) {
		console.error('Error: ' + err);
		return undefined;
	}
};

export const getMediaStream = async (
	mediaOptions: any,
	typeMedia: number = TypeGetMedia.userMedia
) => {
	try {
		return typeMedia === TypeGetMedia.userMedia
			? getUserMedia(mediaOptions)
			: getDisplayMedia(mediaOptions);
	} catch (e) {
    console.log("getMediaStream line: 44",e)
		return undefined;
	}
};

export const getMediaStreamTrack = (
	stream: MediaStream,
	type: number = TypeTrack.audio
): MediaStreamTrack => {
	return type === TypeTrack.audio
		? stream.getAudioTracks()[0]
		: stream.getVideoTracks()[0];
};

export const replaceTrack = () => {};
