import React from 'react'
import { io, Socket } from 'socket.io-client'
import ChatVideoCall from '../../components/pages/videocall/chatVideoCall'

import VideoCallAction from '../../components/pages/videocall/videoCallAction'
import VideoCallRaseHandReceive from '../../components/pages/videocall/videoCallRaseHand'
import { CONSTRAIN_DISPLAY_MEDIA, CONSTRAIN_USER_MEDIA, getMediaStream, getMediaStreamTrack } from '../../helpers/rtc'
import { PC_CONFIG } from '../../helpers/webrtc'
import { TypeGetMedia, TypeTrack } from '../../interfaces/rtc'
import { SOCKET_EVENTS, SOCKET_EVENTS2 } from '../../socket/events'
import { useVideoCallActionContext } from '../../stores/videoCallAction/videoCallAction.context'
import { saveAs } from 'file-saver';
import { useRoomContext } from '../../stores/room/room.context'
import { useRouter } from 'next/router'
import Peer from "simple-peer";


// const HOST_SOCKET = "https://artist.jnweb.tk/"
const HOST_SOCKET = "https://askany-socket.tk"
// 5000...
// const roomState.idRoom = '12345678'

interface IPeerRef {
  peerID: string;
  peer: Peer.Instance
}

const Video = ({ peer }: { peer: Peer.Instance }) => {
  const ref = React.useRef<any>();

  React.useEffect(() => {
    peer.on("stream", stream => {
      console.log(`LHA:  ===> file: index.tsx ===> line 36 ===> stream`, stream)
      // if (!ref.current) return;
      ref.current.srcObject = stream;
    })
  }, []);

  return (
    <video playsInline autoPlay ref={ref} />
  );
}

const CloneVideo = () => {

  console.log("Run Component")

  const router = useRouter()

  const [roomState, roomActions] = useRoomContext()

  const localVideoRef = React.useRef<HTMLVideoElement>(null)
  const remoteVideoRef = React.useRef<HTMLVideoElement>(null)

  const peersRef = React.useRef<Array<IPeerRef>>([]);
  const [peers, setPeers] = React.useState<Array<Peer.Instance>>([]);
  console.log(`LHA:  ===> file: index.tsx ===> line 56 ===> peers`, peers)

  const socketRef = React.useRef<Socket>();


  // const [state, actions] = useVideoCallActionContext()
  const setLocalStream = async () => {
    const stream = await getMediaStream(CONSTRAIN_USER_MEDIA, TypeGetMedia.userMedia)
    if (!localVideoRef.current) return;
    if (stream) {
      localVideoRef.current.srcObject = stream
      joinRoomCss()
      return stream
    }
    return null
  }

  const joinRoomCss = () => {
    if (!socketRef.current) {
      console.log("Socket Ref Null 53")
      return;
    }
    console.log("join room")
    socketRef.current.emit(SOCKET_EVENTS2.JOIN_ROOM_CSS, {
      room: "1234",
    });
  }


  const setupWebRTC = async () => {
    const stream = await setLocalStream()

    if (!socketRef.current || !stream) return;
    socketRef.current.on(SOCKET_EVENTS2.SEND_ALL_USER_SSC, (allUsers: Array<string>) => {
      console.log("setupWebRTC", allUsers)
      // if (allUsers.length > 0) {
      const peers: Array<Peer.Instance> = []
      allUsers.forEach(userId => {
        const peer = createPeer(userId, socketRef.current?.id, stream)
        peersRef.current.push({
          peerID: userId,
          peer,
        })
        peers.push(peer);
      })
      setPeers(peers);
      // }
    });
    socketRef.current.on(SOCKET_EVENTS2.SEND_USER_JOINED_SSC, payload => {
      const peer = addPeer(payload.signal, payload.callerID, stream);
      peersRef.current.push({
        peerID: payload.callerID,
        peer,
      })

      setPeers(users => [...users, peer]);
    });

    socketRef.current.on(SOCKET_EVENTS2.RECEIVING_RETURNED_SIGNAL, payload => {
      const item = peersRef.current.find(p => p.peerID === payload.id);
      item?.peer.signal(payload.signal);
    });

  }

  React.useEffect(() => {
    socketRef.current = io(HOST_SOCKET);

    setupWebRTC()

    return () => {
      // alert("Die Component")
      if (socketRef.current) {
        socketRef.current.disconnect();
      }
    };
  }, []);

  function createPeer(userToSignal: any, callerID: any, stream: MediaStream) {
    const peer = new Peer({
      initiator: true,
      trickle: false,
      stream,
    });

    peer.on("signal", signal => {
      if (!socketRef.current) return;
      socketRef.current.emit(SOCKET_EVENTS2.SENDING_SIGNAL_CSS, { userToSignal, callerID, signal })
    })

    return peer;
  }

  function addPeer(incomingSignal: any, callerID: any, stream: MediaStream) {
    const peer = new Peer({
      initiator: false,
      trickle: false,
      stream,
    })

    peer.on("signal", signal => {
      if (!socketRef.current) return;
      socketRef.current.emit(SOCKET_EVENTS2.RETURNING_SIGNAL_CSS, { signal, callerID })
    })

    peer.signal(incomingSignal);

    return peer;
  }

  return (
    <div className="videoCall-main">
      <div className='container'>
        <div className="videoCall-stream">
          <div className="videoCall-video">
            <div className="videoCall-mainVideo">
              <video autoPlay ref={localVideoRef} ></video>
              <div className="videoCall-point"></div>
            </div>
            <div className="videoCall-subVideo">
              <div className="videoCall-subVideo_wrapper">
                {peers.map((e, i) => <Video key={i} peer={e} />)}
                {/* {peers && peers.length > 0 && <Video peer={peers[0]} />} */}
                {/* <video autoPlay className="" ref={remoteVideoRef}></video> */}
                <VideoCallRaseHandReceive />
              </div>
            </div>
            {/* <VideoCallAction toggleMic={toggleMic} shareScreen={shareScreen} toggleCamera={toggleCamera} handleRaseHand={handleRaseHand} handleRecord={startRecording} handleEndCall={handleEndCall} /> */}
          </div>
          <ChatVideoCall />
        </div>

      </div>
    </div>
  )
}

export default CloneVideo
