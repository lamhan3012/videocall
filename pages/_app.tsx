import '../styles/globals.css'
import type { AppProps } from 'next/app'
import React from 'react'
import { useSocketContext } from '../stores'

const HOST_SOCKET = "https://artist.jnweb.tk/"

function MyApp({ Component, pageProps }: AppProps) {
  const [state, actions] = useSocketContext()
  React.useEffect(() => {
    if (!state.socket) {
      actions.connect(HOST_SOCKET)
    }
    return () => {
      if (state.socket) {
        actions.disconnect()
      }
    }
  }, [])
  return <Component {...pageProps} />
}
export default MyApp
