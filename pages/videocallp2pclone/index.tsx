import React from 'react'
import { io, Socket } from 'socket.io-client';
import { PC_CONFIG } from '../../helpers/webrtc';
import { SOCKET_EVENTS } from '../../socket/events';
import { useSocketContext } from '../../stores';
import { useVideoCallContext } from '../../stores/videoCall/videoCall.context';

// const HOST_SOCKET = "http://127.0.0.1:8080/"
const HOST_SOCKET = "https://askany-socket.tk/"
const VideoCallP2P = () => {
  const [cam, setCam] = React.useState<string>()
  const [state, action] = useVideoCallContext()

  const socketRef = React.useRef<Socket>();
  const pcRef = React.useRef<RTCPeerConnection>();
  const localVideoRef = React.useRef<HTMLVideoElement>(null);
  const remoteVideoRef = React.useRef<HTMLVideoElement>(null);

  const setVideoTracks = async () => {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({
        video: true,
        audio: true,
      });
      if (localVideoRef.current) localVideoRef.current.srcObject = stream;
      if (!(pcRef.current && socketRef.current)) return;
      let stream_settings = stream.getVideoTracks()[0].getSettings();

      // actual width & height of the camera video
      let stream_width = stream_settings.width;
      let stream_height = stream_settings.height;

      console.log('Width: ' + stream_width + 'px');
      console.log('Height: ' + stream_height + 'px');
      stream.getTracks().forEach((track) => {
        if (!pcRef.current) return;
        pcRef.current.addTrack(track, stream);
      });
      pcRef.current.onicecandidate = (e) => {
        if (e.candidate) {
          if (!socketRef.current) return;
          console.log("onicecandidate");
          socketRef.current.emit(SOCKET_EVENTS.SEND_CANDIDATE_CSS, e.candidate);
        }
      };
      pcRef.current.oniceconnectionstatechange = (e) => {
        console.log(e);
      };
      pcRef.current.ontrack = (ev) => {
        console.log("add remotetrack success");
        if (remoteVideoRef.current) {
          remoteVideoRef.current.srcObject = ev.streams[0];
        }
      };
      socketRef.current.emit(SOCKET_EVENTS.JOIN_ROOM_CSS, {
        room: "1234",
      });
    } catch (e) {
      console.error(e);
    }
  };

  const createOffer = async () => {
    console.log("create offer");
    if (!(pcRef.current && socketRef.current)) return;
    try {
      const sdp = await pcRef.current.createOffer({
        offerToReceiveAudio: true,
        offerToReceiveVideo: true,
      });
      await pcRef.current.setLocalDescription(new RTCSessionDescription(sdp));
      socketRef.current.emit(SOCKET_EVENTS.OFFER_CSS, sdp);
    } catch (e) {
      console.error(e);
    }
  };

  const createAnswer = async (sdp: RTCSessionDescription) => {
    if (!(pcRef.current && socketRef.current)) return;
    try {
      await pcRef.current.setRemoteDescription(new RTCSessionDescription(sdp));
      console.log("answer set remote description success");
      const mySdp = await pcRef.current.createAnswer({
        offerToReceiveVideo: true,
        offerToReceiveAudio: true,
      });
      console.log("create answer");
      await pcRef.current.setLocalDescription(new RTCSessionDescription(mySdp));
      socketRef.current.emit(SOCKET_EVENTS.ANSWER_CSS, mySdp);
    } catch (e) {
      console.error(e);
    }
  };

  React.useEffect(() => {
    socketRef.current = io(HOST_SOCKET);
    console.log(`LHA:  ===> file: index.tsx ===> line 85 ===> socketRef.current`, socketRef.current)
    pcRef.current = new RTCPeerConnection(PC_CONFIG);

    socketRef.current.on(SOCKET_EVENTS.JOIN_ROOM_SSC, (allUsers: Array<string>) => {
      console.log(allUsers)
      if (allUsers.length > 0) {
        createOffer();
      }
    });

    socketRef.current.on(SOCKET_EVENTS.OFFER_SSC, (sdp: RTCSessionDescription) => {
      //console.log(sdp);
      console.log("get offer");
      createAnswer(sdp);
    });

    socketRef.current.on(SOCKET_EVENTS.ANSWER_SSC, (sdp: RTCSessionDescription) => {
      console.log("get answer");
      if (!pcRef.current) return;
      pcRef.current.setRemoteDescription(new RTCSessionDescription(sdp));
      //console.log(sdp);
    });

    socketRef.current.on(
      SOCKET_EVENTS.SEND_CANDIDATE_SSC,
      async (candidate: RTCIceCandidateInit) => {
        if (!pcRef.current) return;
        await pcRef.current.addIceCandidate(new RTCIceCandidate(candidate));
        console.log("candidate add success");
      }
    );

    setVideoTracks();

    return () => {
      if (socketRef.current) {
        socketRef.current.disconnect();
      }
      if (pcRef.current) {
        pcRef.current.close();
      }
    };
  }, []);

  async function getConnectedDevices(type: any) {
    const devices = await navigator.mediaDevices.enumerateDevices();
    return devices.filter(device => device.kind === type)
  }

  // Open camera with at least minWidth and minHeight capabilities
  async function openCamera(cameraId: any, minWidth: any, minHeight: any) {
    const constraints = {
      'audio': { 'echoCancellation': true },
      'video': {
        'deviceId': cameraId,
        'width': { 'max': minWidth },
        'height': { 'max': minHeight }
      }
    }

    return await navigator.mediaDevices.getUserMedia(constraints);
  }

  React.useEffect(() => {
    const getCamera = async () => {
      const cameras = await getConnectedDevices('videoinput');
      if (cameras && cameras.length > 0) {
        console.log(`LHA:  ===> file: index.tsx ===> line 30 ===> cameras`, cameras)
        setCam(cameras[0].label)
        // Open first available video camera with a resolution of 1280x720 pixels
        const stream = await openCamera(cameras[0].deviceId, 640, 480);
        let { width, height } = stream.getTracks()[0].getSettings();
        console.log(`${width}x${height}`); // 640x480
        console.log(`LHA:  ===> file: index.tsx ===> line 32 ===> stream`, stream)
      }
    }
  }, [])
  return (
    <div>
      <div>{cam}</div>
      <video
        style={{
          width: 240,
          height: 240,
          margin: 5,
          backgroundColor: "black",
        }}
        muted
        ref={localVideoRef}
        autoPlay
      />
      <video
        id="remotevideo"
        style={{
          width: 240,
          height: 240,
          margin: 5,
          backgroundColor: "black",
        }}
        ref={remoteVideoRef}
        autoPlay
      />
    </div>
  )
}
export default VideoCallP2P