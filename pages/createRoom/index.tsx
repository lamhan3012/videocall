import React from 'react'
import { useRoomContext } from '../../stores/room/room.context'
import Link from 'next/link'

const CreateRoom = () => {

  const [state, actions] = useRoomContext()
  const roomNameRef = React.useRef<HTMLInputElement>(null)

  const handleCreateRoom = () => {
    actions.setIdRoom(roomNameRef.current?.value || '')
  }

  return (
    <div className="createRoom" id='room-create'>
      <div className="container">
        <div className='createRoom-box'>
          <div className="createRoom-title">
            <p>Create Room</p>
          </div>

          <div className="createRoom-form">

            <div className="createRoom-items">
              <label htmlFor="room-name">Room Name</label>
              <input type="text" id='room-name' className="form-control rounded-0" placeholder="Room Name" ref={roomNameRef} />
            </div>

            <div className="createRoom-items">
              <button id='create-room' className="btn btn-createRoom" onClick={handleCreateRoom}>Create Room</button>
            </div>

            {state.idRoom !== '' &&
              <div className="createRoom-items" style={{ flexDirection: 'column' }} id='room-created'>
                <div>
                  <p>Room Id: {state.idRoom}</p>
                </div>
                <div>
                  <Link href="/videocall">
                    <a>Click Me</a>
                  </Link>
                </div>
              </div>
            }

          </div>
        </div>
      </div>

    </div>

  )
}

export default CreateRoom
