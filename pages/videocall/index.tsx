import React from 'react'
import { io, Socket } from 'socket.io-client'
import ChatVideoCall from '../../components/pages/videocall/chatVideoCall'

import VideoCallAction from '../../components/pages/videocall/videoCallAction'
import VideoCallRaseHandReceive from '../../components/pages/videocall/videoCallRaseHand'
import { CONSTRAIN_DISPLAY_MEDIA, CONSTRAIN_USER_MEDIA, getMediaStream, getMediaStreamTrack } from '../../helpers/rtc'
import { PC_CONFIG } from '../../helpers/webrtc'
import { TypeGetMedia, TypeTrack } from '../../interfaces/rtc'
import { SOCKET_EVENTS } from '../../socket/events'
import { useVideoCallActionContext } from '../../stores/videoCallAction/videoCallAction.context'
import { saveAs } from 'file-saver';
import { useRoomContext } from '../../stores/room/room.context'
import { useRouter } from 'next/router'

const HOST_SOCKET = "https://askany-socket.tk/"
// const HOST_SOCKET = "http://localhost:5000"
// 5000...
// const roomState.idRoom = '12345678'

const VideoCall = () => {
  console.log(HOST_SOCKET)


  console.log("Run Component")

  const router = useRouter()
  const [state, actions] = useVideoCallActionContext()
  const [roomState, roomActions] = useRoomContext()


  const socketRef = React.useRef<Socket>();

  const recordedStreamRef = React.useRef<Array<Blob>>([])

  const pcRef = React.useRef<RTCPeerConnection>();

  const streamRef = React.useRef<MediaStream>();
  const shareRef = React.useRef<MediaStream>();

  const localVideoRef = React.useRef<HTMLVideoElement>(null);
  const remoteVideoRef = React.useRef<HTMLVideoElement>(null);

  const mediaRecorderRef = React.useRef<MediaRecorder>()

  const setVideoTracks = async () => {
    try {
      const stream = await navigator.mediaDevices.getUserMedia({
        video: true,
        audio: true,
      });
      streamRef.current = stream
      if (localVideoRef.current) localVideoRef.current.srcObject = stream;
      if (!(pcRef.current && socketRef.current)) return;
      let stream_settings = stream.getVideoTracks()[0].getSettings();

      // actual width & height of the camera video
      // let stream_width = stream_settings.width;
      // let stream_height = stream_settings.height;

      //get video and audio
      stream.getTracks().forEach((track: MediaStreamTrack) => {
        console.log("track in add track", track)
        if (!pcRef.current) return;
        pcRef.current.addTrack(track, stream);
      });
      console.log("stream.getTracks()", stream.getTracks())
      console.log("pcRef.current", pcRef.current)
      pcRef.current.onicecandidate = (e) => {
        if (e.candidate) {
          if (!socketRef.current) return;
          console.log("onicecandidate");
          socketRef.current.emit(SOCKET_EVENTS.SEND_CANDIDATE_CSS, e.candidate);
        }
      };

      pcRef.current.oniceconnectionstatechange = (e) => {
        console.log(e);
      };

      pcRef.current.ontrack = (ev) => {
        console.log("add remotetrack success", ev);
        if (remoteVideoRef.current) {
          remoteVideoRef.current.srcObject = ev.streams[0];
        }
      };
      socketRef.current.emit(SOCKET_EVENTS.JOIN_ROOM_CSS, {
        room: roomState.idRoom,
      });
    } catch (e) {
      console.error(e);
    }
  };

  const createOffer = async () => {
    if (!(pcRef.current && socketRef.current)) return;
    try {
      const sdp = await pcRef.current.createOffer({
        offerToReceiveAudio: true,
        offerToReceiveVideo: true,
      });
      console.log("createOffer sdp", sdp.sdp?.length)
      console.log("state 104", pcRef.current.connectionState)
      await pcRef.current.setLocalDescription(new RTCSessionDescription(sdp));

      console.log("state 107", pcRef.current.connectionState)

      socketRef.current.emit(SOCKET_EVENTS.OFFER_CSS, sdp);
      console.log(`LHA:  ===> file: index.tsx ===> line 108 ===> socketRef.current`, socketRef.current)
    } catch (e) {
      console.error("createOffer", e);
    }
  };

  const createAnswer = async (sdp: RTCSessionDescription) => {
    if (!(pcRef.current && socketRef.current)) return;
    try {
      console.log("state 117", pcRef.current.connectionState)

      await pcRef.current.setRemoteDescription(new RTCSessionDescription(sdp));
      console.log("state 120", pcRef.current.connectionState)

      // console.log("answer set remote description success",pcRef.current);
      const mySdp = await pcRef.current.createAnswer({
        offerToReceiveVideo: true,
        offerToReceiveAudio: true,
      });
      console.log("create answer", mySdp);
      console.log("state 128", pcRef.current.connectionState)
      await pcRef.current.setLocalDescription(new RTCSessionDescription(mySdp));
      console.log("state 130", pcRef.current.connectionState)

      socketRef.current.emit(SOCKET_EVENTS.ANSWER_CSS, mySdp);
    } catch (e) {
      console.error("createAnswer", e);
    }
  };

  React.useEffect(() => {
    socketRef.current = io(HOST_SOCKET);
    pcRef.current = new RTCPeerConnection(PC_CONFIG);

    socketRef.current.on(SOCKET_EVENTS.JOIN_ROOM_SSC, (allUsers: Array<string>) => {
      console.log("SOCKET_EVENTS.JOIN_ROOM_SSC", allUsers)
      if (allUsers.length > 0) {
        createOffer();
      }
    });

    socketRef.current.on(SOCKET_EVENTS.OFFER_SSC, (sdp: RTCSessionDescription) => {
      //console.log(sdp);
      console.log("OFFER_SSC 142", sdp.sdp.length);
      createAnswer(sdp);
    });

    socketRef.current.on(SOCKET_EVENTS.ANSWER_SSC, (sdp: RTCSessionDescription) => {
      console.log("ANSWER_SSC", sdp.sdp.length);
      if (!pcRef.current) return;
      console.log(`LHA:  ===> file: index.tsx ===> line 152 ===>  pcRef.current`, pcRef.current)
      pcRef.current.setRemoteDescription(new RTCSessionDescription(sdp));
      //console.log(sdp);
    });

    socketRef.current.on(
      SOCKET_EVENTS.SEND_CANDIDATE_SSC,
      async (candidate: RTCIceCandidateInit) => {
        console.log(`LHA:  ===> file: index.tsx ===> line 158 ===> candidate`, candidate)
        console.log("SEND_CANDIDATE_SSC", candidate)
        if (!pcRef.current) return;
        await pcRef.current.addIceCandidate(new RTCIceCandidate(candidate));
        console.log("ANSWER_SSC candidate add success");
      }
    );

    socketRef.current.on(
      SOCKET_EVENTS.RASE_HAND_SSC,
      (payload: any) => {
        console.log(`LHA:  ===> file: index.tsx ===> line 168 ===> payload`, payload)

        actions.setRaseHandReceive(payload.raiseHand)
      }
    );


    socketRef.current.on(
      SOCKET_EVENTS.LEAVE_ROOM_SSC,
      (payload: any) => {
        if (!pcRef.current) return;
        pcRef.current.close()
      }
    );


    setVideoTracks();

    return () => {
      if (socketRef.current) {
        socketRef.current.disconnect();
      }
      if (pcRef.current) {
        pcRef.current.close();

        // pcRef.current = undefined;
      }
      if (streamRef.current) streamRef.current.getTracks().forEach(track => track.stop())
      // shareRef
      if (shareRef.current) shareRef.current.getTracks().forEach(track => track.stop())
      // localVideoRef
      // if (localVideoRef.current) localVideoRef.current.srcObject = null
      // if (remoteVideoRef.current) remoteVideoRef.current.srcObject = null


      alert("Die Component")
    };
  }, []);


  const toggleMic = async () => {
    if (!localVideoRef.current) return

    console.log(streamRef.current?.getAudioTracks()[0])
    if (!streamRef.current) return

    const audioTrack = getMediaStreamTrack(streamRef.current)
    if (audioTrack.enabled) {
      audioTrack.enabled = false
    } else {
      audioTrack.enabled = true
    }
  }

  const toggleCamera = async () => {
    if (localVideoRef.current) {
      console.log(streamRef.current?.getAudioTracks()[0])
      if (streamRef.current) {
        const streamTrack = getMediaStreamTrack(streamRef.current, TypeTrack.video)
        if (streamTrack.enabled) {
          streamTrack.enabled = false
        } else {
          streamTrack.enabled = true
        }
      }
    }
  }

  const shareScreen = async () => {
    const stream = await getMediaStream(CONSTRAIN_DISPLAY_MEDIA, TypeGetMedia.displayMedia)
    if (stream) {
      shareRef.current = stream
      if (!localVideoRef.current) return;
      localVideoRef.current.srcObject = stream

      const track = getMediaStreamTrack(stream, TypeTrack.video)

      if (!pcRef.current) return;

      let sender = pcRef.current.getSenders().find(s => s.track && s.track.kind === track.kind);

      if (!sender) return;

      await sender.replaceTrack(track)


      shareRef.current.getVideoTracks()[0].addEventListener('ended', () => {
        stopSharingScreen();
      });
    }
  }

  const stopSharingScreen = async () => {
    console.log("onClick stopSharing")

    const stream = await getMediaStream(CONSTRAIN_USER_MEDIA, TypeGetMedia.userMedia)
    if (stream) {
      shareRef.current = stream
      if (!localVideoRef.current) return;
      localVideoRef.current.srcObject = stream

      const track = getMediaStreamTrack(stream, TypeTrack.video)
      console.log(`LHA:  ===> file: index.tsx ===> line 268 ===> track`, track)

      if (!pcRef.current) return;

      let sender = pcRef.current.getSenders().find(s => s.track && s.track.kind === track.kind);
      console.log(`LHA:  ===> file: index.tsx ===> line 273 ===> sender`, sender)

      if (!sender) return;

      await sender.replaceTrack(track)
      console.log(`LHA:  ===> file: index.tsx ===> line 278 ===> sender`, sender)


      // shareRef.current.getVideoTracks()[0].addEventListener('ended', () => {
      //   stopSharingScreen();
      // });
    }
    // return new Promise<void>((res, rej) => {
    //   if (shareRef.current)
    //     shareRef.current.getTracks().length ? shareRef.current.getTracks().forEach(track => track.stop()) : '';

    //   res();
    // }).then(() => {

    //   console.log("share Success")
    // }).catch((e) => {
    //   console.error(e);
    // });
  }

  const handleRaseHand = () => {
    if (!socketRef.current) return
    const raseHand = !state.isRaseHandClient

    socketRef.current.emit(SOCKET_EVENTS.RASE_HAND_CSS, { idRoom: roomState.idRoom, raiseHand: raseHand })
    actions.setRaseHandClient(raseHand)
  }
  const startRecording = async () => {
    if (!mediaRecorderRef.current) {
      console.log("1234")
      const stream = await navigator.mediaDevices.getDisplayMedia(CONSTRAIN_DISPLAY_MEDIA);
      const streamAudio = await navigator.mediaDevices.getUserMedia(CONSTRAIN_DISPLAY_MEDIA);
      console.log(`LHA:  ===> file: index.tsx ===> line 261 ===> streamAudio`, streamAudio.getTracks())
      console.log(`LHA:  ===> file: index.tsx ===> line 260 ===> stream`, stream.getTracks())
      stream.addTrack(streamAudio.getTracks()[0])
      console.log(`LHA:  ===> file: index.tsx ===> line 260 ===> stream`, stream.getTracks())

      mediaRecorderRef.current = new MediaRecorder(stream, {
        mimeType: 'video/webm;codecs=vp9'
      });
      // console.log(`LHA:  ===> file: index.tsx ===> line 266 ===> mediaRecorder`, mediaRecorder)

      mediaRecorderRef.current.start(1000);
      // toggleRecordingIcons( true );

      mediaRecorderRef.current.ondataavailable = function (e) {
        console.log("e mediaRecorder", e)
        recordedStreamRef.current.push(e.data);
        console.log("Blob", recordedStreamRef.current)
      };

      mediaRecorderRef.current.onerror = function (e) {
        console.error(e);
      };
      // actions.setRecord(true)
    }
    else {
      if (recordedStreamRef.current.length > 0) {
        mediaRecorderRef.current.stop()
        mediaRecorderRef.current = undefined
        saveRecordedStream(recordedStreamRef.current, "username");

        setTimeout(() => {
          recordedStreamRef.current = [];
          actions.setRecord(false)
          console.log("Record Success")
        }, 3000);

      }
    }
  }

  const saveRecordedStream = (stream: Array<Blob>, user: string) => {
    const blob = new Blob(stream, { type: 'video/webm' });

    const file = new File([blob], `${user}-record.webm`);

    saveAs(file);
  }

  const handleEndCall = () => {
    if (!socketRef.current) return;
    console.log("emit nek")
    socketRef.current.emit(SOCKET_EVENTS.LEAVE_ROOM_CSS, {
      room: roomState.idRoom,
    });
    roomActions.setIdRoom("")
    router.push('/createRoom')
  }

  return (
    <div className="videoCall-main">
      <div className='container'>
        <div className="videoCall-stream">
          <div className="videoCall-video">
            <div className="videoCall-mainVideo">
              <video autoPlay ref={localVideoRef} ></video>
              <div className="videoCall-point"></div>
            </div>
            <div className="videoCall-subVideo">
              <div className="videoCall-subVideo_wrapper">
                <video autoPlay className="" ref={remoteVideoRef}></video>
                <VideoCallRaseHandReceive />
              </div>
            </div>
            <VideoCallAction toggleMic={toggleMic} shareScreen={shareScreen} toggleCamera={toggleCamera} handleRaseHand={handleRaseHand} handleRecord={startRecording} handleEndCall={handleEndCall} />
          </div>
          <ChatVideoCall />
        </div>

      </div>
    </div>
  )
}

export default VideoCall
