import { ActionResponseInterface } from "../interfaces/common";

export function ActionResponseModel(
	error: boolean | null = null,
	message: string | null = null
): ActionResponseInterface | null {
	if (typeof error == "boolean" && typeof message == "string") {
		return { error, message };
	}
	return null;
}
